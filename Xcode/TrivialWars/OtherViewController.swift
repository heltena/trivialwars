//
//  OtherViewController.swift
//  TrivialWars
//
//  Created by Helio Tejedor on 12/3/15.
//  Copyright © 2015 Amaral Lab. All rights reserved.
//

import UIKit

class OtherViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var positiveTextField: UITextField!
    @IBOutlet weak var negativeTextField: UITextField!
    
    var user: User?
    var completion: ((Bool) -> Void)?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let user = user else {
            DispatchQueue.main.async {
                self.dismiss(animated: true) { self.completion?(false) }
            }
            return
        }
        
        
        titleLabel.text = user.name
        positiveTextField.text = "\(user.positive)"
        negativeTextField.text = "\(user.negative)"
    }
    
    @IBAction func saveTouched(_ sender: AnyObject) {
        guard let user = user else { return }
        if let positiveTextValue = positiveTextField.text,
            let negativeTextValue = negativeTextField.text,
            let positiveValue = Int(positiveTextValue),
            let negativeValue = Int(negativeTextValue)
        {
            ServerSession.sharedInstance.changeValues(user, positive: positiveValue, negative: negativeValue) {
                status in
                DispatchQueue.main.async {
                    if status {
                        user.positive = positiveValue
                        user.negative = negativeValue
                        self.dismiss(animated: true) { self.completion?(true) }
                    } else {
                        let alert = UIAlertController(title: "Values", message: "Cannot connect to the server", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        } else {
            let alert = UIAlertController(title: "Values", message: "Bad Values", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func backgroundTouched(_ sender: AnyObject) {
        dismiss(animated: true) { self.completion?(false) }
    }
}
