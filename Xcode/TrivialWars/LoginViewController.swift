//
//  LoginViewController.swift
//  TrivialWars
//
//  Created by Helio Tejedor on 11/25/15.
//  Copyright © 2015 Amaral Lab. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
 
    override func viewDidLoad() {
        usernameTextField.text = "sophia"
        passwordTextField.text = ""
    }
    
    @IBAction func loginTouched(_ sender: AnyObject) {
        guard let username = usernameTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        ServerSession.sharedInstance.connect(user: username, password: password) { state in
            if state {
                DispatchQueue.main.async {
                    self.login()
                }
            }
        }
    }
    
    func login() {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "StartViewController") else { return }
        present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func unwindToLoginMenu(_ sender: UIStoryboardSegue) {
    }

}
