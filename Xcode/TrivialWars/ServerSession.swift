//
//  ServerSession.swift
//  ICUCheckList
//
//  Created by Helio Tejedor on 11/8/15.
//  Copyright © 2015 Heltena. All rights reserved.
//

import UIKit

enum PatientOrderType {
    case name
    case room
}

enum RequestMethodType {
    case get
    case post
}

class ServerSession: NSObject, URLSessionDelegate {

#if false
    let host = "127.0.0.1"
    let port = 8000
    let scheme = NSURLProtectionSpaceHTTP
#else
    let host = "siurana.chem-eng.northwestern.edu"
    let port = 443
    let scheme = NSURLProtectionSpaceHTTPS
#endif
    
    let api = "game"
    
    static let sharedInstance = ServerSession()
    
    fileprivate var session: URLSession?
    fileprivate var csrfToken: String?
    fileprivate let timeoutInterval: TimeInterval = 1.0

    var isLogged: Bool {
        return session != nil
    }
    
    fileprivate override init() {
    }

    func connect(user: String, password: String, completion: @escaping (Bool) -> Void) {
        let sessionConfiguration = URLSessionConfiguration.ephemeral
        sessionConfiguration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        self.session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
        guard let session = self.session else { completion(false); return }
        
        var baseComponents = URLComponents()
        baseComponents.scheme = scheme
        baseComponents.host = host
        baseComponents.port = port
        guard
            let baseUrl = baseComponents.url,
            let url = URL(string: "\(api)/login", relativeTo: baseUrl)
            else {
                completion(false)
                return
        }
        
        let body = ["username": user, "password": password]
        var request = URLRequest(url: url.absoluteURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: body, options: [])
            request.httpMethod = "POST"
            request.httpShouldHandleCookies = true
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        } catch {
            completion(false)
            return
        }

        let task = session.dataTask(with: request) { data, response, error in
            guard
                error == nil,
                let response = response as? HTTPURLResponse , response.statusCode == 200,
                let data = data,
                let json = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any],
                let status = json["status"] as? Bool,
                status
                else {
                    completion(false)
                    return
            }
            self.csrfToken = nil
            if let headerFields = response.allHeaderFields as? [String: String] {
                let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url)
                for cookie in cookies {
                    if cookie.name == "csrftoken" {
                        self.csrfToken = cookie.value
                    }
                }
            }
            completion(true)
        }
        task.resume()
    }
    
    func logout() {
        let protectionSpace = URLProtectionSpace(host: host, port: port, protocol: scheme, realm: nil, authenticationMethod: nil)
        let credentialStorage = URLCredentialStorage.shared
        if let credentials = credentialStorage.credentials(for: protectionSpace) {
            for value in credentials.values {
                credentialStorage.remove(value, for: protectionSpace)
            }
        }
    }
    
    private func request(path: String, method: RequestMethodType, body: [String: Any]?, completion: @escaping (Bool, [String: Any]?) -> Void) {
        var baseComponents = URLComponents()
        baseComponents.scheme = scheme
        baseComponents.host = host
        baseComponents.port = port

        guard
            let session = self.session,
            let baseUrl = baseComponents.url,
            let url = URL(string: "\(api)\(path)", relativeTo: baseUrl)
            else {
                completion(false, nil)
                return
        }
        
        var request = URLRequest(url: url.absoluteURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
        do {
            switch method {
            case .get:
                request.httpMethod = "GET"
            case .post:
                request.httpMethod = "POST"
                if body != nil || csrfToken != nil {
                    let effBody: [String: Any] = body ?? [:]
                    request.httpBody = try JSONSerialization.data(withJSONObject: effBody, options: [])
                }
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue(csrfToken ?? "", forHTTPHeaderField: "X-CSRFToken")
                request.addValue("https://siurana.chem-eng.northwestern.edu", forHTTPHeaderField: "REFERER")
            }
        } catch {
            completion(false, nil)
            return
        }
        
        let task = session.dataTask(with: request) { data, response, error in
            if let data = data, let s = String(data: data, encoding: .utf8) {
                print("DEBUG: \(s)")
            }
            guard
                error == nil,
                let response = response as? HTTPURLResponse, response.statusCode == 200,
                let data = data,
                let json = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any],
                let status = json["status"] as? Bool, status
                else {
                    completion(false, nil)
                    return
            }
            completion(true, json)
        }
        task.resume()
    }

    func userList(_ completion: @escaping (Bool, [User]?) -> Void) {
        request(path: "/userList", method: .get, body: nil) { status, json in
            guard
                status,
                let json = json,
                let values = json["values"] as? [Any]
                else {
                    completion(false, nil)
                    return
            }
            var result: [User] = []
            for user in values {
                if let user = user as? [String: AnyObject] {
                    result.append(User(json: user))
                }
            }
            completion(true, result)
        }
    }
    
    func addPositive(_ user: User, completion: @escaping (Bool) -> Void) {
        request(path: "/userPos", method: .post, body: ["id": user.id]) { status, json in
            if status {
                user.positive += 1
            }
            completion(status)
        }
    }
    
    func addNegative(_ user: User, completion: @escaping (Bool) -> Void) {
        request(path: "/userNeg", method: .post, body: ["id": user.id]) { status, json in
            if status {
                user.negative += 1
            }
            completion(status)
        }
    }
    
    func changeValues(_ user: User, positive: Int, negative: Int, completion: @escaping (Bool) -> Void) {
        let body = [
            "id": user.id,
            "positive": positive,
            "negative": negative
        ]
        request(path: "/userChange", method: .post, body: body) { status, json in
            if status {
                user.positive = positive
                user.negative = negative
            }
            completion(status)
        }
    }
   
    func startNewTrivia(_ completion: @escaping (Bool) -> Void) {
        request(path: "/triviaNew", method: .post, body: nil) { status, json in
            completion(status)
        }
    }
    
    //MARK: - NSURLSessionDelegate

    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        self.session = nil
        print("didBecomeInvalidWithError")
    }
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        print("URLSessionDidFinishEventsForBackgroundURLSession")
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            if challenge.protectionSpace.host == host && challenge.protectionSpace.serverTrust != nil {
                let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
                completionHandler(.useCredential, credential)
            } else {
                completionHandler(.rejectProtectionSpace, nil)
            }
        } else {
            completionHandler(.cancelAuthenticationChallenge, nil)
        }
    }
    
}
