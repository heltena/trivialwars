//
//  User.swift
//  TrivialWars
//
//  Created by Helio Tejedor on 11/25/15.
//  Copyright © 2015 Amaral Lab. All rights reserved.
//

import UIKit

class User {
    var id: Int
    var name: String
    var positive: Int
    var negative: Int
    
    init(id: Int) {
        self.id = id
        self.name = "Name \(id)"
        self.positive = 0
        self.negative = 0
    }
    
    init(json: Dictionary<String, AnyObject>) {
        id = json["id"] as? Int ?? 0
        name = json["name"] as? String ?? ""
        positive = json["positive"] as? Int ?? 0
        negative = json["negative"] as? Int ?? 0
    }

}
