//
//  UserCell.swift
//  TrivialWars
//
//  Created by Helio Tejedor on 11/25/15.
//  Copyright © 2015 Amaral Lab. All rights reserved.
//

import UIKit

class UserCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var positivesButton: UIButton!
    @IBOutlet weak var negativesButton: UIButton!
    @IBOutlet weak var modifyButton: UIButton!
    
    var delegate: MainViewDelegate?
    
    var user: User? {
        didSet {
            DispatchQueue.main.async {
                self.label.text = self.user?.name ?? "???"
                self.positivesButton.setTitle("+\(self.user?.positive ?? 0)", for: .normal)
                self.negativesButton.setTitle("-\(self.user?.negative ?? 0)", for: .normal)
                self.isEnabled = true
            }
        }
    }
    
    private var isEnabled: Bool = false {
        didSet {
            DispatchQueue.main.async {
                self.positivesButton.isEnabled = self.isEnabled
                self.negativesButton.isEnabled = self.isEnabled
                self.modifyButton.isEnabled = self.isEnabled
            }
        }
    }

    @IBAction func positivesTouched(_ sender: Any) {
        guard let user = user else { return }
        isEnabled = false
        delegate?.mainAddPositive(user: user) { state in
            self.user = user
        }
    }
    
    @IBAction func negativesTouched(_ sender: Any) {
        guard let user = user else { return }
        isEnabled = false
        delegate?.mainAddNegative(user: user) { state in
            self.user = user
        }
    }
    
    @IBAction func modifyTouched(_ sender: Any) {
        guard let user = user else { return }
        isEnabled = false
        delegate?.mainModify(user: user) { state in
            self.user = user
        }
    }
}
