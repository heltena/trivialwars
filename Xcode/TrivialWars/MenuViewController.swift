//
//  MenuViewController.swift
//  TrivialWars
//
//  Created by Helio Tejedor on 11/25/15.
//  Copyright © 2015 Amaral Lab. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBAction func startNewTriviaTouched(_ sender: AnyObject) {
        ServerSession.sharedInstance.startNewTrivia() { state in
            if state {
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func logoutTouched(_ sender: AnyObject) {
        ServerSession.sharedInstance.logout()
        performSegue(withIdentifier: "UnwindToLoginMenu", sender: self)
    }
    
    @IBAction func backgroundTouched(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
}
