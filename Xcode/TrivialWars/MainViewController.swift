//
//  MainViewController.swift
//  TrivialWars
//
//  Created by Helio Tejedor on 11/25/15.
//  Copyright © 2015 Amaral Lab. All rights reserved.
//

import UIKit

protocol MainViewDelegate: NSObjectProtocol {
    func mainAddPositive(user: User, completion: @escaping (Bool) -> Void)
    func mainAddNegative(user: User, completion: @escaping (Bool) -> Void)
    func mainModify(user: User, completion: @escaping (Bool) -> Void)
}

class MainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MainViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var userList: [User] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ServerSession.sharedInstance.userList() { (state, result) in
            if state {
                guard let userList = result else { return }
                self.userList = userList.sorted { lhr, rhr in lhr.name < rhr.name }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    //MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! UserCell
        cell.user = userList[indexPath.item]
        cell.delegate = self
        return cell
    }
   
    //MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.bounds.size
        return CGSize(width: size.width / 5, height: size.height / 4)
    }
    
    //MARK: - MainViewDelegate
    func mainAddPositive(user: User, completion: @escaping (Bool) -> Void) {
        ServerSession.sharedInstance.addPositive(user, completion: completion)
    }
    
    func mainAddNegative(user: User, completion: @escaping (Bool) -> Void) {
        ServerSession.sharedInstance.addNegative(user, completion: completion)
    }
    
    func mainModify(user: User, completion: @escaping (Bool) -> Void) {
        guard let otherViewController = storyboard?.instantiateViewController(withIdentifier: "other") as? OtherViewController else {
            completion(false)
            return
        }
        otherViewController.user = user
        otherViewController.completion = completion
        present(otherViewController, animated: true, completion: nil)
    }

}

