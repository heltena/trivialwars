angular.module('webApp', [])
  .controller('WebController', function($scope, $http) {
    var self = this;
    
    self.users = [];
    self.bests = {};
 
    self.init = function() {
    	self.loadInfo();
    	setInterval(function() {
    		console.log("Timer!");
    		self.loadInfo();
    	}, 500);
    }
    
    self.loadInfo = function() {
    	$http.get('../api/0.1/user/list').success(function(data) {
    		best_positive = -1;
    		best_negative = -1;
    		best_absolute = -1;
    		for (i = 0; i < data.length; i++) {
    			if (best_positive < data[i].positive)
    				best_positive = data[i].positive;
    			if (best_negative < data[i].negative)
    				best_negative = data[i].negative;
    			absolute = data[i].positive - data[i].negative;
    			if (best_absolute < absolute)
    				best_absolute = absolute;
    		}
    		
    		left_users = [];
    		right_users = [];
    		for (i = 0; i < data.length; i++) {
    			absolute = data[i].positive - data[i].negative;
    			if (absolute == best_absolute)
    				data[i].absolute_class = "first best_absolute";
    			else
    				data[i].absolute_class = "first";

    			if (data[i].positive == best_positive)
    				data[i].positive_class = "middle best_positive";
    			else
    				data[i].positive_class = "middle";
    			
    			if (data[i].negative == best_negative)
    				data[i].negative_class = "last best_negative";
    			else
    				data[i].negative_class = "last";
    			
    			if (i < data.length / 2)
    				left_users.push(data[i]);
    			else
    				right_users.push(data[i]);
    		}
    		if (left_users.length > right_users.length)
    			right_users.push({});
    		
    		users = [];
    		for (i = 0; i < left_users.length; i++) {
    			users.push({left: left_users[i], right: right_users[i]});
    		}
    		self.users = users;
    		self.bests = {positive: best_positive, negative: best_negative, absolute: best_absolute};
    	});
    }

    self.init();
  });