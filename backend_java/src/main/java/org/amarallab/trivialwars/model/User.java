package org.amarallab.trivialwars.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the USER database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String name;

	private int negative;

	private int positive;

	public User() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNegative() {
		return this.negative;
	}

	public void setNegative(int negative) {
		this.negative = negative;
	}

	public int getPositive() {
		return this.positive;
	}

	public void setPositive(int positive) {
		this.positive = positive;
	}

}