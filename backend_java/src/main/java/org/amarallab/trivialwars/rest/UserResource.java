package org.amarallab.trivialwars.rest;

import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.amarallab.trivialwars.business.UserDAO;
import org.amarallab.trivialwars.model.User;

@Path("/user")
@DeclareRoles({"admin"})
public class UserResource {

	@EJB
	UserDAO userDAO;
	
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("admin")
	public Response getUserList() {
		List<User> result = userDAO.getUserList();
		return Response.ok(result).build();
	}

	@POST
	@Path("/pos/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("admin")
	public Response addPositive(@PathParam("id") int id) {
		boolean result = userDAO.addPositive(id);
		return Response.ok(result).build();
	}

	@POST
	@Path("/neg/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("admin")
	public Response addNegative(@PathParam("id") int id) {
		boolean result = userDAO.addNegative(id);
		return Response.ok(result).build();
	}

	@POST
	@Path("/changepos/{id}/{value}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("admin")
	public Response changePositive(@PathParam("id") int id, @PathParam("value") int value) {
		boolean result = userDAO.changePositive(id, value);
		return Response.ok(result).build();
	}

	@POST
	@Path("/changeneg/{id}/{value}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("admin")
	public Response changeNegative(@PathParam("id") int id, @PathParam("value") int value) {
		boolean result = userDAO.changeNegative(id, value);
		return Response.ok(result).build();
	}

	@POST
	@Path("/change/{id}/pos/{positive}/neg/{negative}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("admin")
	public Response changeValues(@PathParam("id") int id, @PathParam("positive") int positive, @PathParam("negative") int negative) {
		boolean result = userDAO.changeValues(id, positive, negative);
		return Response.ok(result).build();
	}

}
