package org.amarallab.trivialwars.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.jackson.JacksonFeature;

@ApplicationPath("/api/0.1/")
public class RSApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		final Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(JacksonFeature.class);
		classes.add(LoginResource.class);
		classes.add(TriviaResource.class);
		classes.add(UserResource.class);
		return classes;
	}

}
