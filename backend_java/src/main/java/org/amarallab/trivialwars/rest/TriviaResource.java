package org.amarallab.trivialwars.rest;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.amarallab.trivialwars.business.UserDAO;

@Path("/trivia")
@DeclareRoles({"admin"})
public class TriviaResource {
	
	@EJB
	UserDAO userDAO;

	@POST
	@Path("/new")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("admin")
	public Response newTrivia() {
		boolean result = userDAO.newTrivia();
		return Response.ok(result).build();
	}

}
