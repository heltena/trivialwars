package org.amarallab.trivialwars.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("/")
public class LoginResource {

	@GET
	@Path("/login")
	public Response login() {
		return Response.ok().build();
	}
	
	@GET
	@Path("/logout")
	public Response logout(@Context HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null)
			session.invalidate();
		return Response.ok().build();
	}

}
