package org.amarallab.trivialwars.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.amarallab.trivialwars.model.User;

@Stateless
public class UserDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public List<User> getUserList() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		
		CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);
		Root<User> rootQuery = criteriaQuery.from(User.class);
		criteriaQuery.select(rootQuery);
		criteriaQuery.orderBy(cb.asc(rootQuery.get("name")));
		return entityManager.createQuery(criteriaQuery).getResultList();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean addPositive(int id) {
		User user = entityManager.find(User.class, id);
		if (user == null)
			return false;
		user.setPositive(user.getPositive() + 1);
		entityManager.persist(user);
		return true;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean addNegative(int id) {
		User user = entityManager.find(User.class, id);
		if (user == null)
			return false;
		user.setNegative(user.getNegative() + 1);
		entityManager.persist(user);
		return true;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean changePositive(int id, int value) {
		User user = entityManager.find(User.class, id);
		if (user == null)
			return false;
		if (value < 0)
			return false;
		user.setPositive(value);
		entityManager.persist(user);
		return true;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean changeNegative(int id, int value) {
		User user = entityManager.find(User.class, id);
		if (user == null)
			return false;
		if (value < 0)
			return false;
		user.setNegative(value);
		entityManager.persist(user);
		return true;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean changeValues(int id, int positive, int negative) {
		User user = entityManager.find(User.class, id);
		if (user == null)
			return false;
		if (positive < 0)
			return false;
		if (negative < 0)
			return false;
		user.setPositive(positive);
		user.setNegative(negative);
		entityManager.persist(user);
		return true;
	}	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean newTrivia() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		
		CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);
		Root<User> rootQuery = criteriaQuery.from(User.class);
		criteriaQuery.select(rootQuery);
		
		for (User user: entityManager.createQuery(criteriaQuery).getResultList()) {
			user.setNegative(0);
			user.setPositive(0);
			entityManager.persist(user);
		}
		
		return true;
	}

}
