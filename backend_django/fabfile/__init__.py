from fabric.api import cd, env, task, local, run, settings, sudo
import json
import requests

env.hosts = ["localhost:8000"]
env.environment = "localhost"
env.test_url = "http://localhost:8000"

@task
def siurana():
    env.hosts = ["siurana.chem-eng.northwestern.edu"]
    env.environment = "siurana"
    env.test_url = "https://{}".format(env.hosts[0])

@task
def deploy():
    if env.environment == "siurana":
        with cd("/var/www/TrivialWars"):
            sudo("chown -R heltena:www-data backend_django")
            run("git pull origin master")
        with cd("/var/www/TrivialWars/backend_django"):
            sudo("/var/www/virtualenvs/TrivialWars/bin/pip install -r requirements.txt")
            sudo("/var/www/virtualenvs/TrivialWars/bin/python manage.py migrate --fake")
            sudo("/var/www/virtualenvs/genexpage/bin/python manage.py collectstatic")
        with cd("/var/www/TrivialWars"):        
            sudo("chown -R www-data:www-data backend_django")
        sudo("service apache2 restart")
