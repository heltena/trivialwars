from django.conf.urls import url
import game.views as views

urlpatterns = [
   url(r'^login$', views.game_login),
   url(r'^userList$', views.game_userList),
   url(r'^userListWeb$', views.game_userListWeb),
   url(r'^userPos$', views.game_userPos),
   url(r'^userNeg$', views.game_userNeg),
   url(r'^userChange$', views.game_userChange),
   url(r'^triviaNew$', views.triviaNew)
]