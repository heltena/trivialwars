from django.db import models

# Create your models here.

class Change(models.Model):
    name = models.CharField(max_length=100)
    when = models.DateTimeField(auto_now=True)
    action = models.CharField(max_length=200)
    prev_positive = models.IntegerField(null=True)
    prev_negative = models.IntegerField(null=True)
    positive = models.IntegerField(null=True)
    negative = models.IntegerField(null=True)

class User(models.Model):
    name = models.CharField(max_length=100)
    positive = models.IntegerField()
    negative = models.IntegerField()