from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
import json

from game.models import User, Change

@api_view(['POST'])
@renderer_classes((JSONRenderer,))
def game_login(request):
    body = request.data
    username = body.get('username', None)
    password = body.get('password', None)
    if None in (username, password):
        return Response({"status": False})

    user = authenticate(username=username, password=password)
    if user is None:
        return Response({"status": False})

    v = login(request, user)
    return Response({"status": user.is_authenticated()})


@api_view(['GET'])
@renderer_classes((JSONRenderer,))
def game_userList(request):
    # if not request.user.is_authenticated():
    #     return Response({'status': False})
    result = []
    for user in User.objects.all():
        result.append({
            'id': user.id, 
            'name': user.name,
            'positive': user.positive,
            'negative': user.negative})
    result.sort(key=lambda x: x['name']) 

    change = Change()
    change.name = "ADMIN"
    change.action = "userList"
    change.save()

    return Response({'status': True, 'values': result})


@api_view(['GET'])
@renderer_classes((JSONRenderer,))
def game_userListWeb(request):
    # if not request.user.is_authenticated():
    #     return Response({'status': False})
    result = []
    for user in User.objects.all():
        result.append({
            'id': user.id, 
            'name': user.name,
            'positive': user.positive,
            'negative': user.negative})
    result.sort(key=lambda x: x['name']) 

    return Response({'status': True, 'values': result})


@api_view(['POST'])
@renderer_classes((JSONRenderer,))
def game_userPos(request):
    body = request.data
    user_id = body.get('id', None)
    if user_id is None:
        return Response({'status': False})

    # if not request.user.is_authenticated():
    #     return Response({'status': False})

    try:
        user = User.objects.get(pk=user_id)
        prev_positive = user.negative
        prev_negative = user.positive
        user.positive += 1
        user.save()

        change = Change()
        change.name = user.name
        change.action = "userPos"
        change.prev_positive = prev_positive
        change.prev_negative = prev_negative
        change.positive = user.positive
        change.negative = user.negative
        change.save()
    
        return Response({'status': True})
    except:
        return Response({'status': False})


@api_view(['POST'])
@renderer_classes((JSONRenderer,))
def game_userNeg(request):
    body = request.data
    user_id = body.get('id', None)
    if user_id is None:
        return REsponse({'status': False})

    # if not request.user.is_authenticated():
    #     return Response({'status': False})

    try:
        user = User.objects.get(pk=user_id)
        prev_positive = user.negative
        prev_negative = user.positive
        user.negative += 1
        user.save()

        change = Change()
        change.name = user.name
        change.action = "userNeg"
        change.prev_positive = prev_positive
        change.prev_negative = prev_negative
        change.positive = user.positive
        change.negative = user.negative
        change.save()

        return Response({'status': True})
    except:
        return Response({'status': False})
    
@api_view(['POST'])
@renderer_classes((JSONRenderer,))
def game_userChange(request):
    body = request.data
    user_id = body.get('id', None)
    positive = body.get('positive', None)
    negative = body.get('negative', None)
    if None in (user_id, positive, negative):
        return Response({'status': False})

    # if not request.user.is_authenticated():
    #     return Response({'status': False})

    try:
        user = User.objects.get(pk=user_id)
        prev_positive = user.negative
        prev_negative = user.positive
        user.positive = positive
        user.negative = negative
        user.save()

        change = Change()
        change.name = user.name
        change.action = "userChange"
        change.prev_positive = prev_positive
        change.prev_negative = prev_negative
        change.positive = user.positive
        change.negative = user.negative
        change.save()

        return Response({'status': True})
    except:
        return Response({'status': False})

@api_view(['POST'])
@renderer_classes((JSONRenderer,))
def triviaNew(request):
    try:
        for user in User.objects.all():
            user.positive = 0
            user.negative = 0
            user.save()

        change = Change()
        change.name = "ADMIN"
        change.action = "triviaNew"
        change.save()

        return Response({'status': True})
    except:
        return Response({'status': False}) 
